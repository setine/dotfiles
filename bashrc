#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi

[ -e /opt/optaliases.sh ] && . /opt/optaliases.sh

alias less='less -R'
alias ls='ls --color=auto'
alias vi=vim
alias vj='vim --cmd "let g:ycm_disable=1"'
#alias ack='ack --pager="less -R"'
alias tchromium='killall chromium;chromium --disable-web-security'
alias tmux="TERM=screen-256color-bce tmux"
PS1='[\u@\h \W]\$ '

# Use system clipboard by default
alias xclip='xclip -selection clipboard'
alias sdir='pwd | xclip'
alias ldir='cd `xclip -o`'
alias ijulia='ipython2 notebook --profile julia'
alias mk='make -j8'
alias mki='make install -j8'

if [[ $(hostname) = "uvm-laptop" ]]; then
    # Home
    export LIBKEYPOINT_INSTALL_PREFIX=/home/uvm/Private/.local
    alias uf='cd $HOME/Private/src/uniqfeed-all'
else
    # Uniqfeed
    export LIBKEYPOINT_INSTALL_PREFIX=/home/uvm/.local
    export PATH=$PATH:/opt/utils:/opt/VirtualGL/bin
    alias uf='cd $HOME/ssd/src/uniqfeed-all'

    # Mate rubbish
    alias alacarte='mozo'
    alias gnome-about='mate-about'
    alias baobab='mate-disk-usage-analyzer'
    alias gcalctool='mate-calc'
    alias gnome-control-center='mate-control-center'
    alias evince='atril'
    alias eog='eom'
    alias file-roller='engrampa'
    alias gconftool-2='mateconftool-2'
    alias gconf-editor='mateconf-editor'
    alias gedit='pluma'
    alias metacity='marco'
    alias nautilus='caja'
    alias gnome-screenshot='mate-screenshot'
    alias gnome-terminal='mate-terminal'
    alias zenity='matedialog'
fi
export PATH=$PATH:$LIBKEYPOINT_INSTALL_PREFIX/bin:
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIBKEYPOINT_INSTALL_PREFIX/lib
alias perf_report="perf report -g 'graph,0.5,caller'"
alias git_clean_branches='git branch --merged | grep -v "\*" | grep -v master | grep -v upstream | xargs -n 1 git branch -d'
#alias git_prefix="git filter-branch -f --msg-filter 'perl -pe "s/^/ADA-2287: /"' upstream..HEAD"
